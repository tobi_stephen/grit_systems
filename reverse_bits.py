#Reverse Bits of 32-bit integer
def rev_bit(uint):
	if uint < 0:
		print "Invalid input: Should be non-negative"
		return
	binary = format(uint, '032b') 	#this converts it to 32 bit binary
	rev_binary = binary[::-1]	#to reversing the binary
	rev_uint = int(rev_binary, 2)	#converts back to integer
	return rev_uint

def main():
	print(rev_bit(43261596))	#outputs 964176192

#This should make the program usable like a module
if __name__=='__main__':
	main()