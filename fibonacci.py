#Fibonnacci Sequence

def fib(n):
	#for the initial sequences
	f0 = 0
	f1 = 1
	if n == 0:
		return 0
	elif n == 1:
		return 1
	return fib(n-1) + fib(n-2)


def main():
	#example 
	print(fib(0)) 	#outputs 0
	print(fib(1))	#outputs 1
	print(fib(7))	#outputs 13
	print(fib(15))	#outputs 610
	print(fib(20))	#outputs 6765
		

#This should make the program usable like a module
if __name__=='__main__':
	main()