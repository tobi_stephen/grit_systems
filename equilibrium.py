#Programming problem 
#Equilibrium index

def solution(array, len):
	for index, elem in enumerate(array):
		if index == 0:
			sum_of_lower = 0
		else:
			sum_of_lower = sum(array[:index])
		#print (index, sum_of_lower)

		if index == len - 1:
			sum_of_higher = 0
		else:
			sum_of_higher = sum(array[index+1:])
		#print(index, sum_of_higher)
		
		if sum_of_lower == sum_of_higher:
			return index	

#it returns all the equilibrium index
def solution_all(array, len):
	all_equilibrium_index = []

	for index, elem in enumerate(array):
		if index == 0:
			sum_of_lower = 0
		else:
			sum_of_lower = sum(array[:index])


		if index == len - 1:
			sum_of_higher = 0
		else:
			sum_of_higher = sum(array[index+1:])

		
		if sum_of_lower == sum_of_higher:
			all_equilibrium_index.append(index)

	return all_equilibrium_index


def main():
	print(solution([-1, 3, -4, 5, 1, -6, 2, 1], 8))	#outputs 1
	print(solution_all([-1, 3, -4, 5, 1, -6, 2, 1], 8))	#outputs a list[1,3,7]

#This should make the program usable like a module
if __name__=='__main__':
	main()