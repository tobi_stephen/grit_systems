#maximum value of 2 words in a list

def max_words_length(words):
	max_value = 0

	for word in words:
		init = word
		deep_copy = words[:]	#this makes a deep copy of the original word list
		deep_copy.remove(init)

		#to crosscheck the word with the rest of the word list
		for new_word in deep_copy:
			a = len(word)
			b = len(new_word)
			
			#to check if there are common letters
			index = 0
			flag = True	#indicating no shared character
			if a >= b:	#initial word is longer
				for x in word:
					if x not in new_word:
						continue
					flag = False	#if there are shared characters


			if a < b:	#new word is longer
				for x in new_word:
					if x not in word:
						continue
					flag = False	#if there are shared characters

			if flag:
				if (a*b) > max_value:
					max_value = a*b;	

	print max_value
	return max_value



def main():
	max_words_length(["abcw", "ba2", "foo", "bar", "xtfn", "abcdef"])
	max_words_length(['a', 'aa', 'aaa', 'aaaa'])
	max_words_length(['a', 'ab', 'abc', 'd', 'cd', 'bcd', 'abcd'])

#This should make the program usable like a module
if __name__=='__main__':
	main()