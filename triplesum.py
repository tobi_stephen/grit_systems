#unique triple sum

def triplesum(array):
	tsum = []
	for x in array:
		init1 = x
		deep_copy = array[:]	#this makes a deep copy of the original word list
		deep_copy.remove(init1)

		for y in deep_copy:
			init2 = y
			deeper_copy = deep_copy[:]
			deeper_copy.remove(init2)

			for z in deeper_copy:
				init3 = z 
				if x + y + z == 0:
					temp = [x, y, z]
					tsum.append(temp)

	out = []
	sortedtsum = []
	#it sorts all the sub-elements in the triple sum array
	for a in tsum:
		sortedtsum.append(sorted(a))

	#to find the unique values of the sorted sum
	for a in sortedtsum:
		if a not in out:
			out.append(a)
	print(out)
	return out

def main():
	triplesum([-1, 0, 1, 2, -1, -4])

#This should make the program usable like a module
if __name__=='__main__':
	main()